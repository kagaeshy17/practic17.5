#include <iostream>
#include <cmath> 

using namespace std;

class Vector 
{
private:
    double x;
    double y;
    double z;

public:
    Vector(double x, double y, double z) : x(x), y(y), z(z) 
    {}

    void print() 
    {
        cout << "Vector(" << x << ", " << y << ", " << z << ")" << endl;
    }

    double length() 
    {
        return sqrt(x * x + y * y + z * z);
    }
};

int main() 
{
    Vector v(3.0, 1.0, 6.0);
    v.print();
    cout << "Length: " << v.length() << endl;

    return 0;
}
